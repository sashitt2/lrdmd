function [L, D, R, info] = ldrOpt_trustregion(A, B, p, x0)

% Written by Palash Sashittal, Feb 2018

% ldrOpt_trustregion : finds an approximate solution to the low rank
% matrix approximation problem in the form:
%
% min \|A - XB\|^2
% s.t. X = LDR', 
%
% where L is an orthonormal matrix with p columns, and D is 
% a square matrix of size p x p.
%
% Inputs:
%
% A,B    : data matrices of size m x n
% p      : p is the rank approximation
% x0     : x0 is the initial condition for an manopt object
%          x0.L is initial condition for L
%          x0.R is initial condition for R
%
% Outputs:
%
% L      : optimal solution basis for the problem above,  
%          with L \in R^{m x p} 
% R      : optimal solution basis for the problem above,
%          with L \in R^{m x p}
% D      : approximate p-ranked dynamical matrix
%
% The input data matrices (A,B) must both be size
% m x n, and full column rank, with p \le n \le m
%
%  

    [m, n] = size(A);
    nA = norm(A,'fro')^2;
    assert(p <= min(m, n), 'p must be smaller than the smallest dimension of A.');
    assert(size(A,1) == size(B,1), 'size of A and B do not match.');
    assert(size(A,2) == size(B,2), 'size of A and B do not match.');
    
    % Define the cost and its derivatives on the Grassmann manifold
    %tuple.L = grassmannfactory(m, p);
    %tuple.R = grassmannfactory(m, p);
    tuple.L = grassmanncomplexfactory(m,p);
    tuple.R = grassmanncomplexfactory(m,p);
    
    M = productmanifold(tuple);
    
    problem.M = M;
    problem.cost  = @cost;
    problem.egrad = @egrad;
    problem.ehess = @ehess;
    
    % Cost function
    function f = cost(X)
        L = X.L;
        R = X.R;
        BtR = B'*R;
        %C = orth(B'*R);
        QR = BtR*((BtR'*BtR)\(BtR'));
        %f = nA-norm((L'*A)*C,'fro')^2;
        f = nA-norm((L'*A)*QR,'fro')^2;
    end
    % Euclidean gradient of the cost function
    function g = egrad(X)
        L = X.L;
        R = X.R;
        BtR = B'*R;
        AtL = A'*L;
        QR = BtR*((BtR'*BtR)\(BtR'));
        P = (AtL*AtL')*BtR/(BtR'*BtR);
        
        g.L = -2*A*QR*AtL;
        g.R = -2*B*(P - QR*P);
    end

    % Euclidean Hessian of the cost function
    function h = ehess(X,H)
        L = X.L;
        R = X.R;
        Ldot = H.L;
        Rdot = H.R;
        BtR = B'*R;
        AtL = A'*L;
        BtRdot = B'*Rdot;
        AtLdot = A'*Ldot;
        invRB = inv(BtR'*BtR);
        RB = BtR'*BtR;
        
        QR = BtR*(RB\BtR');
        %QR = BtR*((BtR'*BtR)\(BtR'));
        P = (AtL*AtL')*BtR/RB;
        %P = (AtL*AtL')*BtR/(BtR'*BtR);
        
        temp1 = BtRdot*(RB\BtR') - ...
               BtR*(RB\(BtRdot'*BtR)/RB)*BtR';
           
        QRdot = temp1 + temp1';
        
        %QRdot = BtRdot*invRB*BtR' - ...
        %        BtR*invRB*(BtRdot'*BtR)*invRB*BtR' - ...
        %        BtR*invRB*(BtR'*BtRdot)*invRB*BtR' + ...
        %        BtR*invRB*BtRdot';
        
        
        Pdot = AtLdot*AtL'*BtR*invRB + ...
               AtL*AtLdot'*BtR*invRB + ...
               AtL*AtL'*BtRdot*invRB - ...
               AtL*AtL'*BtR*invRB*(BtRdot'*BtR)*invRB - ...
               AtL*AtL'*BtR*invRB*(BtR'*BtRdot)*invRB;
    
        h.L = -2*A*(QRdot*AtL + QR*AtLdot);
        h.R = -2*B*Pdot + 2*B*(QR*Pdot + QRdot*P);
    end
    
    
    % Execute some checks on the derivatives for early debugging.
    % These things can be commented out of course.
    %checkgradient(problem);
    %pause;
    
    %checkhessian(problem);
    %pause;
    
    %checkhessian(problem);
    %pause;
    
    %checkhessian(problem);
    %pause;
    
    % Issue a call to a solver.
    
    %[X, Xcost, info] = steepestdescent(problem);
    %[X, Xcost, info] = conjugategradient(problem);
    options.maxiter = 50;
    options.maxtime = 300;
    options.maxinner = 500;
    options.tolgradnorm = 10^(-4);
    [X, ~, info] = trustregions(problem, x0, options);
    L = X.L;
    R = X.R;
    
    LtA = L'*A;
    RtB = R'*B;
    D = (LtA * RtB')/(RtB * RtB');    
    
end
