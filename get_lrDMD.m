function [L,M,R] = get_lrDMD(A,B,r,method)

% written by Palash Sashittal, Feb 2018

% X is the low rank approximation
% A,B are the data matrices
% r is the rank of the approximation
% 'method' says which method to use
% dmd - dynamic mode decomposition
% ldr - lr-DMD with subspace projection method
% hyb - alternating between projection and gradient descent
% trustregion - gradient descent using trust-region algorithm

switch method
    
    case 'dmd'
        % here A and B are the data matrices
        disp('performing dmd');
        
        [U,S,V] = svd(B,'econ');
        Ut = U(:,1:r);
        Vt = V(:,1:r);
        St = S(1:r,1:r);
        
        M = (Ut'*A)*Vt*inv(St);
        L = Ut;
        R = L;
        
    case 'ldr'
        % here A and B are the data matrices
        disp('performing ldr');
        
        [L,D,R] = ldrOpt_subProject(A,B,r);
        M = D;
        
    case 'hyb'
        % here A and B are the data matrices
        disp('performing hyb');
        
        [L,D,R] = ldrOpt_hybrid(A,B,r);
        M = D;        
        
    case 'trustregion'
        % here A and B are the data matrices
        disp('performing ldr with trustregion');
        
        [L,~,R] = ldrOpt_subProject(A,B,r);
        x0.L = L;
        x0.R = R;
        [L,D,R] = ldrOpt_trustregion(A,B,r,x0);
        M = D;

end

end