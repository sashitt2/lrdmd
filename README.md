# README #

### What is this repository for? ###

Performs lr-DMD or DMD

### How do I get set up? ###

1. Download ManOpt toolbox form http://www.manopt.org/downloads.html
2. Unzip and copy the whole manopt directory you just downloaded in a location of your choice on disk, say, in /my/directory/.
3. Go to /my/directory/manopt/ at the command prompt and execute importmanopt
4. Now lr-DMD subroutines can be used as well

### Who do I talk to? ###

Contact: sashitt2@illinois.edu