function [L,D,R] = ldrOpt_subProject(A,B,r)

% Written by Palash Sashittal, Feb 2018

% ldrOpt_subProject : finds an approximate solution to the low rank
% matrix approximation problem in the form:
%
% min \|A - XB\|^2
% s.t. X = LDR', 
%
% where L is an orthonormal matrix with r columns, and D is 
% a square matrix of size r x r.
%
% Inputs:
%
% A,B    : data matrices of size p x n
% r      : r is the rank approximation
%
% Outputs:
%
% L      : optimal solution basis for the problem above,  
%          with L \in R^{p x r} 
% R      : optimal solution basis for the problem above,
%          with L \in R^{p x r}
% D      : approximate r-ranked dynamical matrix
%
% The input data matrices (A,B) must both be size
% p x n, and full column rank, with r \le n \le p

%initialize convergence test variables
iterCount = 0;
relError  = 1;
relTol  = 1e-8;
maxIter = 500;
valueOld  = realmax; 

%initialize algorithm parameters
m = size(A,1);
L = zeros(m,r);%RandOrthMat(m,r,1e-6);
R = L;
Rlast = R;
Llast = L;
k = size(L,2);

%C = orth(A'*L);
C = eye(size(A,2),size(L,2));
%C = RandOrthMat(n,r,1e-6);

%print the reporting header
printInfo();

nA = trace(A'*A);


while( abs(relError) > relTol && iterCount < maxIter)
    
    %update counter
    iterCount = iterCount + 1;
    
    %Find optimal L' ignoring constraints on C
    L = get_orth(A*C,k);

    % find approximate R for given constraint
    [U,~,V] = svd(B*(A'*L),0);
    R = U*V';
    
    %compute the image of B'*L
    %C = solve_inner(B'*R,k);
    C = orth(B'*R);
    
    %calculate relative improvement
    valueNew = sqrt((nA - getNorm(L,A,C)^2));
    relError = (valueOld - valueNew)/abs(valueOld);
    valueOld = valueNew; %for next pass
    
    %print reporting information
    printInfo(iterCount,valueNew,relError);
    
    if(relError < 0)
        relError = 0;
        L = Llast; %from the previous iteration
        R = Rlast; %from the previous iteration
    else
        Llast = L;
        Rlast = R;
    end

end

if(iterCount == maxIter)
    warning('Didn''t converge')
end

LtA = L'*A;
RtB = R'*B;
D = (LtA * RtB')/(RtB * RtB');
end


%----------------------------------------------------
%----------------------------------------------------

function printInfo(iterCount,val,relerr)

%print reporting information
% taken from omd code

if(nargin < 1)
    %print the header
    fprintf(1,'\n\nStarting solver: %s\n\n',mfilename);
    fprintf(1,'iterate  |  objective value  |  relative improvement\n');
    fprintf(1,'----------------------------------------------------\n');
else
    fprintf(1,'%4i     |  %0.6e    |  %0.6e \n',iterCount,val,relerr);
end

end


%----------------------------------------------------
%----------------------------------------------------

function X = get_orth(D,k)

tol=1e-6;
%Compute a basis for D
V = orth(D);

%X coincides with the first k right singular vectors
if(size(V,2) < k)
    for i=size(V,2)+1:k
        nrm = 0;
        while nrm<tol
            vi = randn(size(V,1),1) + 1i*randn(size(V,1),1);
            vi = vi - V(:,1:i-1) * ( V(:,1:i-1)' * vi ) ;
            nrm = norm(vi);
        end
        V(:,i) = vi ./ nrm;
    end
end

X = V(:,1:k);
end

%----------------------------------------------------
%----------------------------------------------------

function n = getNorm(L,A,C)

n = norm((L'*A)*C,'fro');
end