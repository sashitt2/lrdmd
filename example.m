% Let A and B be the data matrices
% A = {x_2 | x_3 | ....| x_N}
% B = {x_1 | x_2 | ....| x_{N-1}}
%% perform DMD
tic;
[Ldmd,Ddmd,~] = get_lrDMD(A,B,r,'dmd');
DMDtime = toc;
%% perform lr-DMD
tic;
[Llr,Dlr,Rlr] = get_lrDMD(A,B,r,'trustregion'); % or any other method option
LRtime = toc;
%% plot the eigenvalues
eDMD = eig(Ddmd);
eLR = eig(Dlr*(Rlr'*Llr));

figure
plot(real(eDMD),imag(eDMD),'o','MarkerSize',15,'LineWidth',3);
hold on
plot(real(eLR),imag(eLR),'^','MarkerSize',15,'LineWidth',3);
hold on
theta = 0:pi/50:2*pi;
plot(cos(theta),sin(theta),'-k','LineWidth',3);
axis equal
legend('DMD','lrDMD');
xlabel('Re(\lambda)');
ylabel('Im(\lambda)');
set(gca,'FontSize',30);
%% Before comparing the orthogonal L,R matrices
% rotate the lr subspace to bring them close to POD modes for comparison
[U,~,V] = svd(Llr'*Ldmd);
rotMat = U*V';

rotLlr = Llr*rotMat;

[U,~,V] = svd(Rlr'*Ldmd);
rotMat = U*V';

rotRldr = Rlr*rotMat;